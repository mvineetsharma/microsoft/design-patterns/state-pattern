﻿using System;

// Define the context class, which represents the fruit.
public class Fruit
{
    private FruitState _state;

    public Fruit()
    {
        // Initialize with the default state.
        _state = new FreshState();
    }

    public void Ripen()
    {
        // Change the state to ripe.
        _state = new RipeState();
    }

    public void Rot()
    {
        // Change the state to rotten.
        _state = new RottenState();
    }

    public void Eat()
    {
        // Delegate the behavior to the current state.
        _state.Eat(this);
    }
}

// Define the state interface.
public interface FruitState
{
    void Eat(Fruit fruit);
}

// Implement concrete states.
public class FreshState : FruitState
{
    public void Eat(Fruit fruit)
    {
        Console.WriteLine("This fruit is fresh and delicious.");
    }
}

public class RipeState : FruitState
{
    public void Eat(Fruit fruit)
    {
        Console.WriteLine("This fruit is ripe and sweet.");
    }
}

public class RottenState : FruitState
{
    public void Eat(Fruit fruit)
    {
        Console.WriteLine("This fruit is rotten and should not be eaten.");
    }
}

class Program
{
    static void Main()
    {
        Fruit apple = new Fruit();

        apple.Eat(); // Output: This fruit is fresh and delicious.

        apple.Ripen();
        apple.Eat(); // Output: This fruit is ripe and sweet.

        apple.Rot();
        apple.Eat(); // Output: This fruit is rotten and should not be eaten.
    }
}
